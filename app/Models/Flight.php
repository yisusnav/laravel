<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
	//Quita atributos Created_at Update_at
	public $timestamps = false;

	//Tabla my_flights
    protected $table = 'my_flights';
}
