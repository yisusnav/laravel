<?php

namespace App\Http\Controllers;

use App\Models\Flight;
use Illuminate\Http\Request;

class HolaController extends Controller
{
	public function index(){

		$flights = Flight::all();
		return view('hola', ['flights' => $flights]);
	}

    public function insertar(Request $request)
    {
    	//Select
    	$flight = new Flight;

    	$flight->name = $request->name;

    	$flight->save();

    	echo "Datos enviados";
    	
    }
}
