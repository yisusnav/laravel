<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return view('home');
});

Route::get('/hola', 'HolaController@index');
Route::post('/hola/insertar', 'HolaController@insertar');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
