@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="collg-12">
            <h2>Insertar</h2>
        </div>
        <div class="col-lg-8">
             <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
        </div>
        <div class="col-lg-4">
            <button id="mensaje" class="btn btn-primary"> enviar </button>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h2>Ver registros</h2>
        </div>
        <div class="col-lg-4">
            <a href="{{url('hola')}}" class="btn btn-primary"> enviar </a>
        </div>
    </div>
</div>
@endsection
