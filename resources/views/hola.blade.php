@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			@foreach ($flights as $flight)
				<div class="col-12">
					<span> {{$flight->name}} </span>
				</div>
			@endforeach
		</div>
	</div>
@endsection